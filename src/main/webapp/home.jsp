<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%--
  Pulling user information from DB to create cookies and login check
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Welcome</title>
    <link href="sources/styles.css" rel="stylesheet">
</head>
<body>
<img src="sources/SecureLogo.png" class="centerImage">
<br/>
<%
    String[] displayCookies = {"username","firstName","lastName","email"};
	List<String> list = new ArrayList<String>();
    Cookie cookie;
    Cookie[] cookies;
	boolean isValidLogin = false;
	cookies = request.getCookies();
	StringBuilder output = new StringBuilder();

	output.append("<h2>Welcome to the system! You are securely logged in. Here is your user information:</h2>");

	for(int i = 0; i<displayCookies.length;i++)
    {
		list.add(displayCookies[i]);
    }

	if(cookies != null)
    {
		//out.print("I see cookies exist with " + cookies.length + " in length<br>");
        output.append("<table>");
        for (int i = 0; i < cookies.length; i++)
        {
            cookie = cookies[i];
            //out.print("Cookie " + cookie.getName() + "<br>Value: " + cookie.getValue() + "<br>");
            if ((cookie.getName().equals("loggedIn")) &&  (cookie.getValue().equals("True")))
            {
				//out.print("Found loggedIn cookie<br>");
				isValidLogin = true;
            }
            else
            {
				//out.print("adding to output<br>");
                if(list.contains(cookie.getName()))
                {
                    output.append("<tr><td>").append(cookie.getName()).append("</td><td> = ").append(
                            cookie.getValue()).append("</td></tr>");
                }
            }
        }
		output.append("</table><br>");
        output.append("Click <a href=index.jsp style=\"color: red\">HERE</a> to logout");
    }

    if(isValidLogin)
    {
        //out.print("Valid login should output the output variable<br>");
        out.print(output.toString());
    }
    else
    {
        response.sendRedirect("index.jsp");
        //out.print("I didn't find login cookie should redirect here");
    }

%>
</body>
</html>